package com.cibertec.examenlinea.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the Usuario database table.
 * 
 */
@Entity
@Table(name="Usuario")
@NamedQuery(name="Usuario.findAll", query="SELECT u FROM Usuario u")
public class Usuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String apellido;

	private String email;

	private String estado;

	@Temporal(TemporalType.DATE)
	private Date fechaRegistro;

	private String nombre;

	private String password;

	private String rol;

	private String usuario;

	//bi-directional many-to-one association to Resultado
	@OneToMany(mappedBy="usuario")
	private List<Resultado> resultados;

	//bi-directional many-to-many association to Curso
	@ManyToMany
	@JoinTable(
		name="UsuarioCurso"
		, joinColumns={
			@JoinColumn(name="idUsuario")
			}
		, inverseJoinColumns={
			@JoinColumn(name="idCurso")
			}
		)
	private List<Curso> cursos;

	//bi-directional many-to-one association to UsuarioCurso
	@OneToMany(mappedBy="usuario")
	private List<UsuarioCurso> usuarioCursos;

	public Usuario() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getApellido() {
		return this.apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFechaRegistro() {
		return this.fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRol() {
		return this.rol;
	}

	public void setRol(String rol) {
		this.rol = rol;
	}

	public String getUsuario() {
		return this.usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public List<Resultado> getResultados() {
		return this.resultados;
	}

	public void setResultados(List<Resultado> resultados) {
		this.resultados = resultados;
	}

	public Resultado addResultado(Resultado resultado) {
		getResultados().add(resultado);
		resultado.setUsuario(this);

		return resultado;
	}

	public Resultado removeResultado(Resultado resultado) {
		getResultados().remove(resultado);
		resultado.setUsuario(null);

		return resultado;
	}

	public List<Curso> getCursos() {
		return this.cursos;
	}

	public void setCursos(List<Curso> cursos) {
		this.cursos = cursos;
	}

	public List<UsuarioCurso> getUsuarioCursos() {
		return this.usuarioCursos;
	}

	public void setUsuarioCursos(List<UsuarioCurso> usuarioCursos) {
		this.usuarioCursos = usuarioCursos;
	}

	public UsuarioCurso addUsuarioCurso(UsuarioCurso usuarioCurso) {
		getUsuarioCursos().add(usuarioCurso);
		usuarioCurso.setUsuario(this);

		return usuarioCurso;
	}

	public UsuarioCurso removeUsuarioCurso(UsuarioCurso usuarioCurso) {
		getUsuarioCursos().remove(usuarioCurso);
		usuarioCurso.setUsuario(null);

		return usuarioCurso;
	}
	
	public Date getCurrentDate() {
		return new Date();
	}

}