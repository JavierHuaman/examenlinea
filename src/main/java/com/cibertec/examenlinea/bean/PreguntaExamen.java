package com.cibertec.examenlinea.bean;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the PreguntaExamen database table.
 * 
 */
@Entity
@Table(name="PreguntaExamen")
@NamedQuery(name="PreguntaExamen.findAll", query="SELECT p FROM PreguntaExamen p")
public class PreguntaExamen implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private int puntos;

	//bi-directional many-to-one association to Examen
	@ManyToOne
	@JoinColumn(name="idExamen")
	private Examen examen;

	//bi-directional many-to-one association to Pregunta
	@ManyToOne
	@JoinColumn(name="idPregunta")
	private Pregunta pregunta;

	public PreguntaExamen() {
		pregunta = new Pregunta();
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPuntos() {
		return this.puntos;
	}

	public void setPuntos(int puntos) {
		this.puntos = puntos;
	}

	public Examen getExamen() {
		return this.examen;
	}

	public void setExamen(Examen examen) {
		this.examen = examen;
	}

	public Pregunta getPregunta() {
		return this.pregunta;
	}

	public void setPregunta(Pregunta pregunta) {
		this.pregunta = pregunta;
	}

	@Override
	public String toString() {
		return "PreguntaExamen [id=" + id + ", puntos=" + puntos + ", examen=" + examen + ", pregunta=" + pregunta
				+ "]";
	}

}