package com.cibertec.examenlinea.bean;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the Opcion database table.
 * 
 */
@Entity
@Table(name="Opcion")
@NamedQuery(name="Opcion.findAll", query="SELECT o FROM Opcion o")
public class Opcion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Lob
	private String descripcion;

	@Column(name="es_correcto")
	private boolean esCorrecto;

	//bi-directional many-to-one association to Pregunta
	@ManyToOne
	@JoinColumn(name="idPregunta")
	private Pregunta pregunta;

	//bi-directional many-to-many association to Resultado
	@ManyToMany(mappedBy="opcions")
	private List<Resultado> resultados;

	//bi-directional many-to-one association to ResultadoOpcion
	@OneToMany(mappedBy="opcion")
	private List<ResultadoOpcion> resultadoOpcions;

	public Opcion() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public boolean getEsCorrecto() {
		return this.esCorrecto;
	}

	public void setEsCorrecto(boolean esCorrecto) {
		this.esCorrecto = esCorrecto;
	}

	public Pregunta getPregunta() {
		return this.pregunta;
	}

	public void setPregunta(Pregunta pregunta) {
		this.pregunta = pregunta;
	}

	public List<Resultado> getResultados() {
		return this.resultados;
	}

	public void setResultados(List<Resultado> resultados) {
		this.resultados = resultados;
	}

	public List<ResultadoOpcion> getResultadoOpcions() {
		return this.resultadoOpcions;
	}

	public void setResultadoOpcions(List<ResultadoOpcion> resultadoOpcions) {
		this.resultadoOpcions = resultadoOpcions;
	}

	public ResultadoOpcion addResultadoOpcion(ResultadoOpcion resultadoOpcion) {
		getResultadoOpcions().add(resultadoOpcion);
		resultadoOpcion.setOpcion(this);

		return resultadoOpcion;
	}

	public ResultadoOpcion removeResultadoOpcion(ResultadoOpcion resultadoOpcion) {
		getResultadoOpcions().remove(resultadoOpcion);
		resultadoOpcion.setOpcion(null);

		return resultadoOpcion;
	}

	@Override
	public String toString() {
		return "Opcion [id=" + id + ", descripcion=" + descripcion + ", esCorrecto=" + esCorrecto + "]";
	}

}