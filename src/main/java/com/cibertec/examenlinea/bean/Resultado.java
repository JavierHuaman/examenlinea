package com.cibertec.examenlinea.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the Resultado database table.
 * 
 */
@Entity
@Table(name="Resultado")
@NamedQuery(name="Resultado.findAll", query="SELECT r FROM Resultado r")
public class Resultado implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Temporal(TemporalType.TIME)
	private Date duracion;

	private String estado;

	@Temporal(TemporalType.DATE)
	private Date fecha;

	private double porcentajeResuelto;

	//bi-directional many-to-one association to Examen
	@ManyToOne
	@JoinColumn(name="idExamen")
	private Examen examen;

	//bi-directional many-to-many association to Opcion
	@ManyToMany
	@JoinTable(
		name="ResultadoOpcion"
		, joinColumns={
			@JoinColumn(name="idResultado")
			}
		, inverseJoinColumns={
			@JoinColumn(name="idOpcion")
			}
		)
	private List<Opcion> opcions;

	//bi-directional many-to-one association to Usuario
	@ManyToOne
	@JoinColumn(name="idUsuario")
	private Usuario usuario;

	//bi-directional many-to-one association to ResultadoOpcion
	@OneToMany(mappedBy="resultado")
	private List<ResultadoOpcion> resultadoOpcions;

	public Resultado() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDuracion() {
		return this.duracion;
	}

	public void setDuracion(Date duracion) {
		this.duracion = duracion;
	}

	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public double getPorcentajeResuelto() {
		return this.porcentajeResuelto;
	}

	public void setPorcentajeResuelto(double porcentajeResuelto) {
		this.porcentajeResuelto = porcentajeResuelto;
	}

	public Examen getExamen() {
		return this.examen;
	}

	public void setExamen(Examen examen) {
		this.examen = examen;
	}

	public List<Opcion> getOpcions() {
		return this.opcions;
	}

	public void setOpcions(List<Opcion> opcions) {
		this.opcions = opcions;
	}

	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<ResultadoOpcion> getResultadoOpcions() {
		return this.resultadoOpcions;
	}

	public void setResultadoOpcions(List<ResultadoOpcion> resultadoOpcions) {
		this.resultadoOpcions = resultadoOpcions;
	}

	public ResultadoOpcion addResultadoOpcion(ResultadoOpcion resultadoOpcion) {
		getResultadoOpcions().add(resultadoOpcion);
		resultadoOpcion.setResultado(this);

		return resultadoOpcion;
	}

	public ResultadoOpcion removeResultadoOpcion(ResultadoOpcion resultadoOpcion) {
		getResultadoOpcions().remove(resultadoOpcion);
		resultadoOpcion.setResultado(null);

		return resultadoOpcion;
	}

}