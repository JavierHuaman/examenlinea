package com.cibertec.examenlinea.bean;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the UsuarioCurso database table.
 * 
 */
@Entity
@Table(name="UsuarioCurso")
@NamedQuery(name="UsuarioCurso.findAll", query="SELECT u FROM UsuarioCurso u")
public class UsuarioCurso implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	//bi-directional many-to-one association to Curso
	@ManyToOne
	@JoinColumn(name="idCurso")
	private Curso curso;

	//bi-directional many-to-one association to Usuario
	@ManyToOne
	@JoinColumn(name="idUsuario")
	private Usuario usuario;

	@Transient
	private double avance;
	
	@Transient
	private String estado;

	public UsuarioCurso() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Curso getCurso() {
		return this.curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public void setAvance(double avance) {
		this.avance = avance;		
	}
	
	public double getAvance() {
		return avance;
	}

	public void setEstado(String estado) {
		this.estado = estado;		
	}
	
	public String getEstado() {
		return estado;
	}

}