package com.cibertec.examenlinea.bean;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the TipoPregunta database table.
 * 
 */
@Entity
@Table(name="TipoPregunta")
@NamedQuery(name="TipoPregunta.findAll", query="SELECT t FROM TipoPregunta t")
public class TipoPregunta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Lob
	private String descripcion;

	//bi-directional many-to-one association to Pregunta
	@OneToMany(mappedBy="tipoPregunta")
	private List<Pregunta> preguntas;

	public TipoPregunta() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<Pregunta> getPreguntas() {
		return this.preguntas;
	}

	public void setPreguntas(List<Pregunta> preguntas) {
		this.preguntas = preguntas;
	}

	public Pregunta addPregunta(Pregunta pregunta) {
		getPreguntas().add(pregunta);
		pregunta.setTipoPregunta(this);

		return pregunta;
	}

	public Pregunta removePregunta(Pregunta pregunta) {
		getPreguntas().remove(pregunta);
		pregunta.setTipoPregunta(null);

		return pregunta;
	}

}