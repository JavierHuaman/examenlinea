package com.cibertec.examenlinea.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the Examen database table.
 * 
 */
@Entity
@Table(name="Examen")
@NamedQuery(name="Examen.findAll", query="SELECT e FROM Examen e")
public class Examen implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Temporal(TemporalType.TIME)
	private Date duracion;

	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaFin;

	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaInicio;

	@Lob
	private String instrucciones;

	private String nombre;

	private double porcentajeAprobatorio;

	//bi-directional many-to-one association to Curso
	@ManyToOne
	@JoinColumn(name="idCurso")
	private Curso curso;

	//bi-directional many-to-one association to PreguntaExamen
	@OneToMany(mappedBy="examen")
	private List<PreguntaExamen> preguntaExamens;

	//bi-directional many-to-one association to Resultado
	@OneToMany(mappedBy="examen")
	private List<Resultado> resultados;

	public Examen() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDuracion() {
		return this.duracion;
	}

	public void setDuracion(Date duracion) {
		this.duracion = duracion;
	}

	public Date getFechaFin() {
		return this.fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public Date getFechaInicio() {
		return this.fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getInstrucciones() {
		return this.instrucciones;
	}

	public void setInstrucciones(String instrucciones) {
		this.instrucciones = instrucciones;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public double getPorcentajeAprobatorio() {
		return this.porcentajeAprobatorio;
	}

	public void setPorcentajeAprobatorio(double porcentajeAprobatorio) {
		this.porcentajeAprobatorio = porcentajeAprobatorio;
	}

	public Curso getCurso() {
		return this.curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	public List<PreguntaExamen> getPreguntaExamens() {
		return this.preguntaExamens;
	}

	public void setPreguntaExamens(List<PreguntaExamen> preguntaExamens) {
		this.preguntaExamens = preguntaExamens;
	}

	public PreguntaExamen addPreguntaExamen(PreguntaExamen preguntaExamen) {
		getPreguntaExamens().add(preguntaExamen);
		preguntaExamen.setExamen(this);

		return preguntaExamen;
	}

	public PreguntaExamen removePreguntaExamen(PreguntaExamen preguntaExamen) {
		getPreguntaExamens().remove(preguntaExamen);
		preguntaExamen.setExamen(null);

		return preguntaExamen;
	}

	public List<Resultado> getResultados() {
		return this.resultados;
	}

	public void setResultados(List<Resultado> resultados) {
		this.resultados = resultados;
	}

	public Resultado addResultado(Resultado resultado) {
		getResultados().add(resultado);
		resultado.setExamen(this);

		return resultado;
	}

	public Resultado removeResultado(Resultado resultado) {
		getResultados().remove(resultado);
		resultado.setExamen(null);

		return resultado;
	}
	
	public double totalSatisfactorio() {
		double total = 0;
		for (PreguntaExamen preguntaExamen : getPreguntaExamens()) {
			total += preguntaExamen.getPuntos();
		}
		return total;
	}

	@Override
	public String toString() {
		return "Examen [id=" + id + ", duracion=" + duracion + ", fechaFin=" + fechaFin + ", fechaInicio=" + fechaInicio
				+ ", instrucciones=" + instrucciones + ", nombre=" + nombre + ", porcentajeAprobatorio="
				+ porcentajeAprobatorio + ", curso=" + curso + "]";
	}

}