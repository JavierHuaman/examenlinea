package com.cibertec.examenlinea.bean;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the Curso database table.
 * 
 */
@Entity
@Table(name="Curso")
@NamedQuery(name="Curso.findAll", query="SELECT c FROM Curso c")
public class Curso implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String descripcion;

	private String estado;

	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha;

	private String nombre;

	//bi-directional many-to-one association to Examen
	@OneToMany(mappedBy="curso")
	private List<Examen> examens;

	//bi-directional many-to-one association to Pregunta
	@OneToMany(mappedBy="curso", fetch=FetchType.EAGER)
	private List<Pregunta> preguntas;

	//bi-directional many-to-many association to Usuario
	@ManyToMany(mappedBy="cursos")
	private List<Usuario> usuarios;

	//bi-directional many-to-one association to UsuarioCurso
	@OneToMany(mappedBy="curso")
	private List<UsuarioCurso> usuarioCursos;
	
	public Curso() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Examen> getExamens() {
		return this.examens;
	}

	public void setExamens(List<Examen> examens) {
		this.examens = examens;
	}

	public Examen addExamen(Examen examen) {
		getExamens().add(examen);
		examen.setCurso(this);

		return examen;
	}

	public Examen removeExamen(Examen examen) {
		getExamens().remove(examen);
		examen.setCurso(null);

		return examen;
	}

	public List<Pregunta> getPreguntas() {
		return this.preguntas;
	}

	public void setPreguntas(List<Pregunta> preguntas) {
		this.preguntas = preguntas;
	}

	public Pregunta addPregunta(Pregunta pregunta) {
		getPreguntas().add(pregunta);
		pregunta.setCurso(this);

		return pregunta;
	}

	public Pregunta removePregunta(Pregunta pregunta) {
		getPreguntas().remove(pregunta);
		pregunta.setCurso(null);

		return pregunta;
	}

	public List<Usuario> getUsuarios() {
		return this.usuarios;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	public List<UsuarioCurso> getUsuarioCursos() {
		return this.usuarioCursos;
	}

	public void setUsuarioCursos(List<UsuarioCurso> usuarioCursos) {
		this.usuarioCursos = usuarioCursos;
	}

	public UsuarioCurso addUsuarioCurso(UsuarioCurso usuarioCurso) {
		getUsuarioCursos().add(usuarioCurso);
		usuarioCurso.setCurso(this);

		return usuarioCurso;
	}

	public UsuarioCurso removeUsuarioCurso(UsuarioCurso usuarioCurso) {
		getUsuarioCursos().remove(usuarioCurso);
		usuarioCurso.setCurso(null);

		return usuarioCurso;
	}

	@Override
	public String toString() {
		return "Curso [id=" + id + ", descripcion=" + descripcion + ", estado=" + estado + ", fecha=" + fecha
				+ ", nombre=" + nombre + ", usuarios=" + usuarios + "]";
	}

}