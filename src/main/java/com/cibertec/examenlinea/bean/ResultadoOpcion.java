package com.cibertec.examenlinea.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;


/**
 * The persistent class for the ResultadoOpcion database table.
 * 
 */
@Entity
@Table(name="ResultadoOpcion")
@NamedQuery(name="ResultadoOpcion.findAll", query="SELECT r FROM ResultadoOpcion r")
public class ResultadoOpcion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	//bi-directional many-to-one association to Opcion
	@ManyToOne
	@JoinColumn(name="idOpcion")
	private Opcion opcion;

	//bi-directional many-to-one association to Resultado
	@ManyToOne
	@JoinColumn(name="idResultado")
	private Resultado resultado;
	
	//bi-directional many-to-one association to Pregunta
	@ManyToOne
	@JoinColumn(name="idPregunta")
	private Pregunta pregunta;
	
	private String opcionTexto;
	
	@Transient
	private Opcion pseudoOpcion;
	
	@Transient
	private List<String> multiOpcion;
	
	@Transient
	private boolean correcto;

	public ResultadoOpcion() {
		multiOpcion = new ArrayList<String>();
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Opcion getOpcion() {
		return this.opcion;
	}

	public void setOpcion(Opcion opcion) {
		this.opcion = opcion;
	}

	public Resultado getResultado() {
		return this.resultado;
	}

	public void setResultado(Resultado resultado) {
		this.resultado = resultado;
	}

	public Pregunta getPregunta() {
		return pregunta;
	}

	public void setPregunta(Pregunta pregunta) {
		this.pregunta = pregunta;
	}
	
	@Override
	public String toString() {
		return "ResultadoOpcion [id=" + id + ", opcion=" + opcion + ", pregunta=" + pregunta + "]";
	}

	public Opcion getPseudoOpcion() {
		return pseudoOpcion;
	}

	public void setPseudoOpcion(Opcion pseudoOpcion) {
		this.pseudoOpcion = pseudoOpcion;
	}

	public List<String> getMultiOpcion() {
		return multiOpcion;
	}

	public void setMultiOpcion(List<String> multiOpcion) {
		this.multiOpcion = multiOpcion;
	}

	public String getOpcionTexto() {
		return opcionTexto;
	}

	public void setOpcionTexto(String opcionTexto) {
		this.opcionTexto = opcionTexto;
	}

	public boolean isCorrecto() {
		return correcto;
	}

	public void setCorrecto(boolean correcto) {
		this.correcto = correcto;
	}

}