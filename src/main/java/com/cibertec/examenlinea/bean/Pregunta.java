package com.cibertec.examenlinea.bean;

import java.io.Serializable;
import javax.persistence.*;

import java.util.ArrayList;
import java.util.List;


/**
 * The persistent class for the Pregunta database table.
 * 
 */
@Entity
@Table(name="Pregunta")
@NamedQuery(name="Pregunta.findAll", query="SELECT p FROM Pregunta p")
public class Pregunta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Lob
	private String descripcion;

	//bi-directional many-to-one association to Opcion
	@OneToMany(mappedBy="pregunta")
	private List<Opcion> opcions;

	//bi-directional many-to-one association to Curso
	@ManyToOne
	@JoinColumn(name="idCurso")
	private Curso curso;

	//bi-directional many-to-one association to TipoPregunta
	@ManyToOne
	@JoinColumn(name="idTipoPregunta")
	private TipoPregunta tipoPregunta;

	//bi-directional many-to-one association to PreguntaExamen
	@OneToMany(mappedBy="pregunta")
	private List<PreguntaExamen> preguntaExamens;

	//bi-directional many-to-one association to PreguntaExamen
	@OneToMany(mappedBy="pregunta")
	private List<ResultadoOpcion> resultaOpcions;

	public Pregunta() {
		tipoPregunta = new TipoPregunta();
		opcions = new ArrayList<Opcion>();
		curso = new Curso();
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<Opcion> getOpcions() {
		return this.opcions;
	}

	public void setOpcions(List<Opcion> opcions) {
		this.opcions = opcions;
	}

	public Opcion addOpcion(Opcion opcion) {
		getOpcions().add(opcion);
		opcion.setPregunta(this);

		return opcion;
	}

	public Opcion removeOpcion(Opcion opcion) {
		getOpcions().remove(opcion);
		opcion.setPregunta(null);

		return opcion;
	}

	public Curso getCurso() {
		return this.curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	public TipoPregunta getTipoPregunta() {
		return this.tipoPregunta;
	}

	public void setTipoPregunta(TipoPregunta tipoPregunta) {
		this.tipoPregunta = tipoPregunta;
	}

	public List<PreguntaExamen> getPreguntaExamens() {
		return this.preguntaExamens;
	}

	public void setPreguntaExamens(List<PreguntaExamen> preguntaExamens) {
		this.preguntaExamens = preguntaExamens;
	}

	public PreguntaExamen addPreguntaExamen(PreguntaExamen preguntaExamen) {
		getPreguntaExamens().add(preguntaExamen);
		preguntaExamen.setPregunta(this);

		return preguntaExamen;
	}

	public PreguntaExamen removePreguntaExamen(PreguntaExamen preguntaExamen) {
		getPreguntaExamens().remove(preguntaExamen);
		preguntaExamen.setPregunta(null);

		return preguntaExamen;
	}
	
	public List<Opcion> opcionesCorrectas() {
		List<Opcion> opcions = new ArrayList<Opcion>();
		for (Opcion opt: getOpcions()) {
			if (opt.getEsCorrecto()) {
				opcions.add(opt);
			}
		}
		return opcions;
	}
	
	public List<ResultadoOpcion> getResultaOpcions() {
		return resultaOpcions;
	}

	public void setResultaOpcions(List<ResultadoOpcion> resultaOpcions) {
		this.resultaOpcions = resultaOpcions;
	}

	@Override
	public String toString() {
		return "Pregunta [id=" + id + ", descripcion=" + descripcion + ", curso=" + curso + ", tipoPregunta="
				+ tipoPregunta + "]";
	}

}