package com.cibertec.examenlinea.service;

import java.util.List;

import javax.persistence.Query;

import com.cibertec.examenlinea.bean.Opcion;

public class OpcionService extends AbstractService<Opcion> {

	@SuppressWarnings("unchecked")
	public List<Opcion> opcionesxpregunta(int idPregunta) {
		List<Opcion> opciones = null;
		Query query = this.manager.createQuery("SELECT e FROM Opcion e where e.pregunta.id = ?1");
		query.setParameter(1, idPregunta);
		opciones = (List<Opcion>) query.getResultList();		
		return opciones;
	}
	
	public void eliminarxpregunta(int idPregunta) {
		manager.getTransaction().begin();
		Query query = this.manager.createQuery("DELETE FROM Opcion e where e.pregunta.id = ?1");
		query.setParameter(1, idPregunta);
		query.executeUpdate();	
		manager.flush();
		manager.getTransaction().commit();
	}
}
