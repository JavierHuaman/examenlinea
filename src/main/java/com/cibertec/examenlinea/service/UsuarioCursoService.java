package com.cibertec.examenlinea.service;

import java.util.List;

import javax.persistence.Query;

import com.cibertec.examenlinea.bean.Usuario;
import com.cibertec.examenlinea.bean.UsuarioCurso;

public class UsuarioCursoService extends AbstractService<UsuarioCurso> {

	@SuppressWarnings("unchecked")
	public boolean existisusuarioycurso(int idUsuario, int idCurso) {
		boolean exists = false;
		try {
			String query = "SELECT u FROM UsuarioCurso u WHERE u.usuario.id = ?1  and u.curso.id = ?2";
			Query q = manager.createQuery(query);
			q.setParameter(1, idUsuario);
			q.setParameter(2, idCurso);
			List<UsuarioCurso> usuarios = q.getResultList();
			exists = !usuarios.isEmpty();	
		} catch (Exception e) {
			// Error
		} 
		return exists;		
	}

	@SuppressWarnings("unchecked")
	public List<UsuarioCurso> listaporusuario(int idUsuario) {
		List<UsuarioCurso> cursos = null;
		Query query = this.manager.createQuery("SELECT e FROM UsuarioCurso e where e.usuario.id = ?1 and e.curso.estado = ?2");
		query.setParameter(1, idUsuario);
		query.setParameter(2, "1");
		cursos = (List<UsuarioCurso>) query.getResultList();		
		return cursos;
	}
	
	@SuppressWarnings("unchecked")
	public List<Usuario> listaporcurso(int idCurso) {
		List<Usuario> usuarios = null;
		Query query = this.manager.createQuery("SELECT e.usuario FROM UsuarioCurso e where e.curso.id = ?1");
		query.setParameter(1, idCurso);
		usuarios = (List<Usuario>) query.getResultList();		
		return usuarios;
	}
	
}
