package com.cibertec.examenlinea.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import com.cibertec.examenlinea.bean.Resultado;

public class ResultadoService extends AbstractService<Resultado> {

	@SuppressWarnings("unchecked")
	public Resultado existeporusuarioyexamen(int idUsuario, int idExamen) {
		Resultado resultado = null;
		try {
			String query = "SELECT u FROM Resultado u WHERE u.usuario.id = ?1  and u.examen.id = ?2";
			Query q = manager.createQuery(query);
			q.setParameter(1, idUsuario);
			q.setParameter(2, idExamen);
			List<Resultado> resultados = q.getResultList();
			if (!resultados.isEmpty()) {
				resultado = resultados.get(0);
			}
		} catch (Exception e) {
			// Error
		} 
		return resultado;	
	}
	

	@SuppressWarnings("unchecked")
	public List<Resultado> existeporcursooexamen(int idCurso, int idExamen) {
		List<Resultado> resultados = new ArrayList<Resultado>();		
		try {
			Query q = null;
			String query = "SELECT u FROM Resultado u ";
			if (idCurso > 0 && idExamen <= 0) { // SOLO POR CURSO
				query += "WHERE u.examen.curso.id = ?1";
				q = manager.createQuery(query);
				q.setParameter(1, idCurso);			
			} else if (idCurso > 0 && idExamen > 0) { // POR CURSO Y POR EXAMEN
				query += "WHERE u.examen.curso.id = ?1 and u.examen.id = ?2";
				q = manager.createQuery(query);
				q.setParameter(1, idCurso);
				q.setParameter(2, idExamen);
			} else {
				q = manager.createQuery(query);				
			}
			resultados = q.getResultList();
		} catch (Exception e) {
			// Error
		} 
		return resultados;	
	}
	
}
