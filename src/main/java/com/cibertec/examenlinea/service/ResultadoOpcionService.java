package com.cibertec.examenlinea.service;

import java.util.List;

import javax.persistence.Query;

import com.cibertec.examenlinea.bean.ResultadoOpcion;

public class ResultadoOpcionService extends AbstractService<ResultadoOpcion> {
	
	@SuppressWarnings("unchecked")
	public List<ResultadoOpcion> detallexresultado(int idResultado) {
		List<ResultadoOpcion> resultados = null;
		try {
			String query = "SELECT u FROM ResultadoOpcion u WHERE u.resultado.id = ?1";
			Query q = manager.createQuery(query);
			q.setParameter(1, idResultado);
			resultados = q.getResultList();
		} catch (Exception e) {
			// Error
		} 
		return resultados;	
	}
}
