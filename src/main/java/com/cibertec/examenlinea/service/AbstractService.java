package com.cibertec.examenlinea.service;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

public abstract class AbstractService<T> {
	
	@PersistenceContext
    protected EntityManager manager;

    private Class<T> type;
    
	@SuppressWarnings("unchecked")
	public AbstractService() {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("examenlinea");
		this.manager = factory.createEntityManager();
        Type t = getClass().getGenericSuperclass();
        ParameterizedType pt = (ParameterizedType) t;
        
        type = (Class<T>) pt.getActualTypeArguments()[0]; // get class of T
	}
	
	public List<T> listar() {
    	String name_query = type.getSimpleName() + ".findAll";
    	TypedQuery<T> query = manager.createNamedQuery(name_query, type);
    	return query.getResultList();
	}
	
	public T registrar(final T entidad) throws Exception {
    	try {
        	manager.getTransaction().begin();
        	manager.persist(entidad);
        	manager.flush();
        	manager.getTransaction().commit();			
		} catch (Exception e) {
			manager.getTransaction().rollback();
			e.printStackTrace();
			throw e;
		}
        return entidad;		
	}
	
	public T actualizar(final T entidad) throws Exception {
    	T n = null;
    	try {
    		manager.getTransaction().begin();
    		n = manager.merge(entidad); 
    		manager.flush();
    		manager.getTransaction().commit();			
		} catch (Exception e) {
			manager.getTransaction().rollback();
			throw e;
		}
        return n;	
	}
	
	public void eliminar(final int id) throws Exception {
    	try {
    		manager.getTransaction().begin();
    		manager.remove(manager.getReference(type, id));		
    		manager.flush();
    		manager.getTransaction().commit();
		} catch (Exception e) {
			throw e;
		}
		
	}
	
	public T obtener(final int id) {
        return (T) manager.find(type, id);
	}
	
}
