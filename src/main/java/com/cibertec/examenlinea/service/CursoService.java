package com.cibertec.examenlinea.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import com.cibertec.examenlinea.bean.Curso;

public class CursoService extends AbstractService<Curso>{
	
	@SuppressWarnings("unchecked")
	public List<Curso> listarActivos() {
		List<Curso> cursos = new ArrayList<Curso>();
		Query q = manager.createQuery("SELECT c FROM Curso c where c.estado = '1'");
		cursos = q.getResultList();		
		return cursos;
	}
}
