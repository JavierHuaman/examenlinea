package com.cibertec.examenlinea.service;

import java.util.List;

import javax.persistence.Query;

import com.cibertec.examenlinea.bean.PreguntaExamen;

public class PreguntaExamenService extends AbstractService<PreguntaExamen> {

	@SuppressWarnings("unchecked")
	public List<PreguntaExamen> detallexexamen(int idExamen) {
		List<PreguntaExamen> examenes = null;
		Query query = this.manager.createQuery("SELECT e FROM PreguntaExamen e where e.examen.id = ?1");
		query.setParameter(1, idExamen);
		examenes = (List<PreguntaExamen>) query.getResultList();		
		return examenes;
	}
	
	public void eliminarxexamen(int idExamen) {
		this.manager.getTransaction().begin();
		Query query = this.manager.createQuery("DELETE FROM PreguntaExamen e where e.examen.id = ?1");
		query.setParameter(1, idExamen);
		query.executeUpdate();
		this.manager.getTransaction().commit();
	}
	
	@SuppressWarnings("unchecked")
	public PreguntaExamen obtenerporexamenypregunta(int idExamen, int idPregunta) {
		PreguntaExamen preguntaExamen = null;
		try {
			Query query = this.manager.createQuery("SELECT e FROM PreguntaExamen e where e.examen.id = ?1 and e.pregunta.id = ?2");
			query.setParameter(1, idExamen);
			query.setParameter(2, idPregunta);
			List<PreguntaExamen> examenes = (List<PreguntaExamen>) query.getResultList();	
			if(!examenes.isEmpty()) {
				preguntaExamen = examenes.get(0);
			}			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return preguntaExamen;
	}
}
