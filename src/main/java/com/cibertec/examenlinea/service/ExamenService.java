package com.cibertec.examenlinea.service;

import java.util.List;

import javax.persistence.Query;

import com.cibertec.examenlinea.bean.Examen;

public class ExamenService extends AbstractService<Examen> {

	@SuppressWarnings("unchecked")
	public List<Examen> examenesxcurso(int idCurso) {
		List<Examen> examenes = null;
		Query query = this.manager.createQuery("SELECT e FROM Examen e where e.curso.id = ?1");
		query.setParameter(1, idCurso);
		examenes = (List<Examen>) query.getResultList();		
		return examenes;
	}
}
