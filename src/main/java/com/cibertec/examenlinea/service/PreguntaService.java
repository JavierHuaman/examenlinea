package com.cibertec.examenlinea.service;

import java.util.List;

import javax.persistence.Query;

import com.cibertec.examenlinea.bean.Pregunta;

public class PreguntaService extends AbstractService<Pregunta>{

	@SuppressWarnings("unchecked")
	public List<Pregunta> detallexcurso(int idCurso) {
		List<Pregunta> examenes = null;
		Query query = this.manager.createQuery("SELECT e FROM Pregunta e where e.curso.id = ?1");
		query.setParameter(1, idCurso);
		examenes = (List<Pregunta>) query.getResultList();		
		return examenes;
	}
	
	@SuppressWarnings("unchecked")
	public List<Pregunta> detallexdescripcion(String descripcion) {
		List<Pregunta> examenes = null;
		Query query = this.manager.createQuery("SELECT e FROM Pregunta e where e.descripcion LIKE ?1");
		query.setParameter(1, "%" + descripcion + "%" );
		examenes = (List<Pregunta>) query.getResultList();		
		return examenes;
	}
}
