package com.cibertec.examenlinea.service;

import java.util.List;

import javax.persistence.Query;

import com.cibertec.examenlinea.bean.Usuario;

public class UsuarioService extends AbstractService<Usuario> {
	
	@SuppressWarnings("unchecked")
	public Usuario login(final String username, final String password) {
		Usuario usuario = null;
		try {
			String query = "SELECT u FROM Usuario u WHERE u.usuario = ?1  and u.password = ?2";
			Query q = manager.createQuery(query);
			q.setParameter(1, username);
			q.setParameter(2, password);
			List<Usuario> usuarios = q.getResultList();
			if (!usuarios.isEmpty()) {
				usuario = usuarios.get(0);
			}			
		} catch (Exception e) {
			// Error
		} 
		return usuario;
	}
}
