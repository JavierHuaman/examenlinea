package com.cibertec.examenlinea.controller;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import com.cibertec.examenlinea.bean.Usuario;
import com.cibertec.examenlinea.service.UsuarioService;


@RequestScoped
@ManagedBean(name = "authController")
public class AuthController {
	
	private String username;
	private String password;
	private UsuarioService usuarioService = new UsuarioService();
	
	public String logIn() {
		String redirect = null;
		try {
			Usuario usuario = usuarioService.login(username, password);
			if (usuario != null) {
				FacesContext.getCurrentInstance()
					.getExternalContext()
						.getSessionMap().put("usuario", usuario);
				redirect = "/pages/principal?faces-redirect=true";				
			} else {
				FacesContext.getCurrentInstance().addMessage(null, 
						new FacesMessage(FacesMessage.SEVERITY_WARN, "Aviso", "Credenciales invalidas"));	
			}
		} catch(Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, 
					new FacesMessage(FacesMessage.SEVERITY_FATAL, "Aviso", "Error !"));			
		}
		return redirect;
	}
	
	public void checkSession() {
		try {
			Usuario usuario = (Usuario) FacesContext.getCurrentInstance()
											.getExternalContext()
												.getSessionMap().get("usuario");
			FacesContext context = FacesContext.getCurrentInstance();
			context.getExternalContext().getFlash().setKeepMessages(true);
			if (usuario == null) {
//				FacesContext.getCurrentInstance().getExternalContext().redirect("pages/index?faces-redirect=true");
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Aviso", 
								"Cerro sesión."));
				context.getExternalContext().redirect("/examenlinea/?faces-redirect=true");
			} 
			
			
		} catch (Exception e) {
			// Error !!!
		}		
	}
	
	public void logOut() {
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


}
