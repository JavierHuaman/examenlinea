package com.cibertec.examenlinea.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;

import org.primefaces.component.api.UIData;
import org.primefaces.component.selectonemenu.SelectOneMenu;

import com.cibertec.examenlinea.bean.Curso;
import com.cibertec.examenlinea.bean.Opcion;
import com.cibertec.examenlinea.bean.Pregunta;
import com.cibertec.examenlinea.bean.TipoPregunta;
import com.cibertec.examenlinea.service.CursoService;
import com.cibertec.examenlinea.service.OpcionService;
import com.cibertec.examenlinea.service.PreguntaService;
import com.cibertec.examenlinea.service.TipoPreguntaService;

@ViewScoped
@ManagedBean(name = "preguntaController")
public class PreguntaController {

	private PreguntaService preguntaService = new PreguntaService();
	private CursoService cursoService = new CursoService();
	private TipoPreguntaService tipoPreguntaService = new TipoPreguntaService();
	private OpcionService opcionService = new OpcionService();
	
	private Pregunta pregunta;
	private List<Pregunta> preguntas;
	private List<Curso> cursos;
	private List<TipoPregunta> tipoPreguntas;
	private UIData dttPreguntas;	

	// Agregar pregunta.
	private int idTP;
	private int preguntaId;
	private int idCurso;
	
	// respuestas
	private String verdaderoFalso; // si el tipo de pregunta es verdadero falso.
	private String rptCompleta; // si el tipo de pregunta es texto. (o como alternativa)
	private List<String> alternativas; // si el tipo de pregunta es seleccion unica o multiple.
	private String alternativa;
	private List<String> alternativasSeleccionadas;
	
	@PostConstruct
	public void init() {
		pregunta = new Pregunta();
		preguntas = preguntaService.listar();
		cursos = cursoService.listar();
		tipoPreguntas = tipoPreguntaService.listar();
		alternativas = new ArrayList<String>();
		alternativasSeleccionadas = new ArrayList<String>();
	}

	public String registrar() {
		String redirect = null;
		try {
			pregunta = preguntaService.registrar(pregunta);
			switch (idTP) {
				case 1:
					String[] opciones = {"Verdadero", "Falso"};
					for (String op : opciones) {
						Opcion opcion = new Opcion();
						opcion.setDescripcion(op);
						opcion.setEsCorrecto(verdaderoFalso.equals(op.toLowerCase()));
						opcion.setPregunta(pregunta);
						opcionService.registrar(opcion);		
					}
					break;
				case 2:
					for (String alt : alternativas) {
						Opcion opcion = new Opcion();
						opcion.setDescripcion(alt);
						opcion.setEsCorrecto(alternativa.equals(alt));
						opcion.setPregunta(pregunta);
						opcionService.registrar(opcion);	
					}
					break;
				case 3:
					for (String alt : alternativas) {
						Opcion opcion = new Opcion();
						opcion.setDescripcion(alt);
						opcion.setEsCorrecto(alternativasSeleccionadas.indexOf(alt) != -1);
						opcion.setPregunta(pregunta);
						opcionService.registrar(opcion);	
					}				
					break;
				case 4:
					Opcion opcion = new Opcion();
					opcion.setDescripcion(rptCompleta);
					opcion.setEsCorrecto(true);
					opcion.setPregunta(pregunta);
					opcionService.registrar(opcion);
				default:
					break;
			}
			FacesContext context = FacesContext.getCurrentInstance();
			context.getExternalContext().getFlash().setKeepMessages(true);
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Aviso", "Guardado satisfactoriamente"));			
			redirect = "/pages/admin/pregunta/editar?faces-redirect=true&preguntaId=" + pregunta.getId();
		} catch (Exception e) {
			FacesContext.getCurrentInstance()
			.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Aviso", "Error al grabar los datos"));
		}
		return redirect;
	}
	
	public String actualizar() {
		String redirect = null;
		try {
			pregunta = preguntaService.actualizar(pregunta);
			opcionService.eliminarxpregunta(pregunta.getId());
			switch (idTP) {
				case 1:
					String[] opciones = {"Verdadero", "Falso"};
					for (String op : opciones) {
						Opcion opcion = new Opcion();
						opcion.setDescripcion(op);
						opcion.setEsCorrecto(verdaderoFalso.equals(op.toLowerCase()));
						opcion.setPregunta(pregunta);
						opcionService.registrar(opcion);		
					}
					break;
				case 2:
					for (String alt : alternativas) {
						Opcion opcion = new Opcion();
						opcion.setDescripcion(alt);
						opcion.setEsCorrecto(alternativa.equals(alt));
						opcion.setPregunta(pregunta);
						opcionService.registrar(opcion);	
					}
					break;
				case 3:
					for (String alt : alternativas) {
						Opcion opcion = new Opcion();
						opcion.setDescripcion(alt);
						opcion.setEsCorrecto(alternativasSeleccionadas.indexOf(alt) != -1);
						opcion.setPregunta(pregunta);
						opcionService.registrar(opcion);	
					}				
					break;
				case 4:
					Opcion opcion = new Opcion();
					opcion.setDescripcion(rptCompleta);
					opcion.setEsCorrecto(true);
					opcion.setPregunta(pregunta);
					opcionService.registrar(opcion);
				default:
					break;
			}			
			
			FacesContext.getCurrentInstance()
				.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Aviso", "Actualizado satisfactoriamente"));		
			redirect = "/pages/admin/pregunta/editar?faces-redirect=true&preguntaId=" + pregunta.getId();		
		} catch (Exception e) {
			e.printStackTrace();
			FacesContext.getCurrentInstance()
				.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Aviso", "Error al grabar los datos"));
		}
		return redirect;
	}

	// Evento del select tipo de pregunta
	public void selectionChanged(final AjaxBehaviorEvent event) {
		SelectOneMenu oneMenu = (SelectOneMenu) event.getSource();
		this.idTP = Integer.parseInt(oneMenu.getValue().toString());
		this.verdaderoFalso = null;
		this.rptCompleta = null;
		this.alternativas = new ArrayList<String>();
		this.alternativasSeleccionadas = new ArrayList<String>();
	}
		
	public void agregarAlternativa() {
		alternativas.add(rptCompleta);
		this.rptCompleta = null;
	}
	
	public void buscarxcurso() {
		if (idCurso == 0) {
			preguntas = preguntaService.listar();
		} else {
			preguntas = preguntaService.detallexcurso(idCurso);
		}
	}

	public void eliminarAlternativa(int index) {
		alternativas.remove(index);
	}
	
	public Pregunta getPregunta() {
		return pregunta;
	}

	public void setPregunta(Pregunta pregunta) {
		this.pregunta = pregunta;
	}

	public List<Pregunta> getPreguntas() {
		return preguntas;
	}

	public void setPreguntas(List<Pregunta> preguntas) {
		this.preguntas = preguntas;
	}

	public UIData getDttPreguntas() {
		return dttPreguntas;
	}

	public void setDttPreguntas(UIData dttPreguntas) {
		this.dttPreguntas = dttPreguntas;
	}

	public List<Curso> getCursos() {
		return cursos;
	}

	public void setCursos(List<Curso> cursos) {
		this.cursos = cursos;
	}

	public List<TipoPregunta> getTipoPreguntas() {
		return tipoPreguntas;
	}

	public void setTipoPreguntas(List<TipoPregunta> tipoPreguntas) {
		this.tipoPreguntas = tipoPreguntas;
	}	

	public List<String> getAlternativas() {
		return alternativas;
	}

	public void setAlternativas(List<String> alternativas) {
		this.alternativas = alternativas;
	}

	public String getAlternativa() {
		return alternativa;
	}

	public void setAlternativa(String alternativa) {
		this.alternativa = alternativa;
	}

	public List<String> getAlternativasSeleccionadas() {
		return alternativasSeleccionadas;
	}

	public void setAlternativasSeleccionadas(List<String> alternativasSeleccionadas) {
		this.alternativasSeleccionadas = alternativasSeleccionadas;
	}
	public int getIdTP() {
		return idTP;
	}

	public void setIdTP(int idTP) {
		this.idTP = idTP;
	}

	public String getVerdaderoFalso() {
		return verdaderoFalso;
	}

	public void setVerdaderoFalso(String verdaderoFalso) {
		this.verdaderoFalso = verdaderoFalso;
	}

	public String getRptCompleta() {
		return rptCompleta;
	}

	public void setRptCompleta(String rptCompleta) {
		this.rptCompleta = rptCompleta;
	}

	public int getPreguntaId() {
		return preguntaId;
	}

	public void setPreguntaId(int preguntaId) {
		this.pregunta = preguntaService.obtener(preguntaId);
		List<Opcion> opciones = opcionService.opcionesxpregunta(preguntaId);
		this.idTP = pregunta.getTipoPregunta().getId();
		switch (idTP) {
			case 1:
				for (Opcion opcion : opciones) {
					if(opcion.getEsCorrecto()) {
						this.verdaderoFalso = opcion.getDescripcion().toLowerCase();
					}
				}
				break;
			case 2:
				for (Opcion opcion : opciones) {
					this.alternativas.add(opcion.getDescripcion());
					if(opcion.getEsCorrecto()) {
						this.alternativa  = opcion.getDescripcion();
					}
				}
				break;
			case 3:
				for (Opcion opcion : opciones) {
					this.alternativas.add(opcion.getDescripcion());
					if(opcion.getEsCorrecto()) {
						this.alternativasSeleccionadas.add(opcion.getDescripcion());
					}
				}		
				break;
			case 4:
				for (Opcion opcion : opciones) {
					if(opcion.getEsCorrecto()) {
						this.rptCompleta = opcion.getDescripcion().toLowerCase();
					}
				}
			default:
				break;
		}
		
		this.preguntaId = preguntaId;
	}

	public int getIdCurso() {
		return idCurso;
	}

	public void setIdCurso(int idCurso) {
		this.idCurso = idCurso;
	}
	
}
