package com.cibertec.examenlinea.controller;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import com.cibertec.examenlinea.bean.Examen;
import com.cibertec.examenlinea.bean.Resultado;
import com.cibertec.examenlinea.bean.Usuario;
import com.cibertec.examenlinea.bean.UsuarioCurso;
import com.cibertec.examenlinea.service.ExamenService;
import com.cibertec.examenlinea.service.ResultadoService;
import com.cibertec.examenlinea.service.UsuarioCursoService;

@RequestScoped
@ManagedBean(name = "perfilController")
public class PerfilController {

	private UsuarioCursoService usuarioCursoService = new UsuarioCursoService();
	private ExamenService examenService = new ExamenService();
	private ResultadoService resultadoService = new ResultadoService();
	private Usuario usuario;
	private List<UsuarioCurso> cursos;
	
	@PostConstruct
	public void init() {
		usuario = (Usuario) FacesContext.getCurrentInstance()
									.getExternalContext()
										.getSessionMap().get("usuario");
		cursos = usuarioCursoService.listaporusuario(usuario.getId());
		for (UsuarioCurso curso : cursos) {
			List<Examen> examenes = examenService.examenesxcurso(curso.getId());
			double total = examenes.size();
			int avance = 0;
			int aprobado = 0;
			for (Examen examen : examenes) {
				Resultado resultado = resultadoService.existeporusuarioyexamen(usuario.getId(), examen.getId());
				if (resultado != null) {
					avance ++;
				}
				if (resultado.getEstado().equals("A")) {
					aprobado ++;
				}
			}
			curso.setAvance((avance/total) * 100);
			curso.setEstado(aprobado > (total/2) ? "Aprobado" : "Desaprobado");
		}
	}
	
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<UsuarioCurso> getCursos() {
		return cursos;
	}

	public void setCursos(List<UsuarioCurso> cursos) {
		this.cursos = cursos;
	}

}
