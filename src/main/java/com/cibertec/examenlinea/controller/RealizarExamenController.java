package com.cibertec.examenlinea.controller;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.cibertec.examenlinea.bean.Examen;
import com.cibertec.examenlinea.bean.Opcion;
import com.cibertec.examenlinea.bean.Pregunta;
import com.cibertec.examenlinea.bean.PreguntaExamen;
import com.cibertec.examenlinea.bean.Resultado;
import com.cibertec.examenlinea.bean.ResultadoOpcion;
import com.cibertec.examenlinea.bean.Usuario;
import com.cibertec.examenlinea.service.ExamenService;
import com.cibertec.examenlinea.service.OpcionService;
import com.cibertec.examenlinea.service.PreguntaExamenService;
import com.cibertec.examenlinea.service.ResultadoOpcionService;
import com.cibertec.examenlinea.service.ResultadoService;

@ViewScoped
@ManagedBean(name = "realizarexamenController")
public class RealizarExamenController {

	private ExamenService examenService = new ExamenService();
	private PreguntaExamenService preguntaExamenService = new PreguntaExamenService();
	private ResultadoService resultadoService = new ResultadoService();
	private ResultadoOpcionService resultadoOpcionService = new ResultadoOpcionService();
	private OpcionService opcionService = new OpcionService();

	private int examenId;
	private Examen examen;
	private Usuario usuario;
	
	private List<ResultadoOpcion> resultadoOpciones;
	private int duracionExamen;
	
	private boolean ya_hizo_examen;
	
	private int tiempoUsado;
	
	@PostConstruct
	public void init() {
		examen = new Examen();
		resultadoOpciones = new ArrayList<ResultadoOpcion>();
		usuario = (Usuario) FacesContext.getCurrentInstance()
								.getExternalContext()
								.getSessionMap().get("usuario");
	}

	public int getExamenId() {
		return examenId;
	}
	
	public String registrar() {
		String redirect = null;
		double acertado = 0;
		List<ResultadoOpcion> toSave = new ArrayList<ResultadoOpcion>();
		for (ResultadoOpcion resultadoOpcion : resultadoOpciones) {
			Pregunta pregunta = resultadoOpcion.getPregunta();
			int idTipoPregunta = pregunta.getTipoPregunta().getId();
			List<Opcion> opcionesCorrectas = pregunta.opcionesCorrectas();
			Opcion correcto = null;
			
			switch (idTipoPregunta) {
				case 1: // verdadero o falso
					correcto = opcionesCorrectas.get(0);
					if(correcto.getId() == resultadoOpcion.getPseudoOpcion().getId()) {
						PreguntaExamen pEx = preguntaExamenService.obtenerporexamenypregunta(examen.getId(), pregunta.getId());
						acertado += pEx.getPuntos();
					}
					toSave.add(crearparaguardar(resultadoOpcion.getPseudoOpcion(), resultadoOpcion.getPregunta()));
					break;
				case 2: // seleccion unica
					correcto = opcionesCorrectas.get(0);
					if(correcto.getId() == resultadoOpcion.getPseudoOpcion().getId()) {
						PreguntaExamen pEx = preguntaExamenService.obtenerporexamenypregunta(examen.getId(), pregunta.getId());
						acertado += pEx.getPuntos();
					}
					toSave.add(crearparaguardar(resultadoOpcion.getPseudoOpcion(), resultadoOpcion.getPregunta()));			
					break;
				case 3: // seleccion multiple
					boolean es_correcto = false;
					for (String sidOpc : resultadoOpcion.getMultiOpcion()) {
						int idOpc = Integer.parseInt(sidOpc);
						for (Opcion opcionCorrecta : opcionesCorrectas) {
							if (idOpc == opcionCorrecta.getId()) {
								es_correcto = true;
								break;
							}
						}
						Opcion opt = new Opcion();
						opt.setId(idOpc);
						toSave.add(crearparaguardar(opt, resultadoOpcion.getPregunta()));
					}
					if (es_correcto) {
						PreguntaExamen pEx = preguntaExamenService.obtenerporexamenypregunta(examen.getId(), pregunta.getId());
						acertado += pEx.getPuntos();							
					}
					break;
				case 4: // texto
					correcto = opcionesCorrectas.get(0);
					if(correcto.getDescripcion().equalsIgnoreCase(resultadoOpcion.getPseudoOpcion().getDescripcion())) {
						PreguntaExamen pEx = preguntaExamenService.obtenerporexamenypregunta(examen.getId(), pregunta.getId());
						acertado += pEx.getPuntos();
					}
					ResultadoOpcion cpr = crearparaguardar(null, resultadoOpcion.getPregunta());
					cpr.setOpcionTexto(resultadoOpcion.getPseudoOpcion().getDescripcion());
					toSave.add(cpr);
					break;	
				default:
					break;
			}
			
		}
		try {
			Resultado resultado = new Resultado();
			resultado.setExamen(examen);
			resultado.setPorcentajeResuelto(acertado/examen.totalSatisfactorio());
			resultado.setEstado(resultado.getPorcentajeResuelto() >= examen.getPorcentajeAprobatorio() ? "A" : "D");
			resultado.setDuracion(new Date(tiempoUsado*1000));
			resultado.setUsuario(usuario);
			resultado.setFecha(new Date());
			resultado = resultadoService.registrar(resultado);
			for (ResultadoOpcion resultadoOpcionTS : toSave) {
				resultadoOpcionTS.setResultado(resultado);
				resultadoOpcionService.registrar(resultadoOpcionTS);
			}
			redirect = "/pages/estudiante/curso/resultado/show.jsf?faces-redirect=true&resultadoId=" + resultado.getId();
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return redirect;
	}
	
	ResultadoOpcion crearparaguardar(Opcion seleccionada, Pregunta pregunta) {
		ResultadoOpcion rseleccionada = new ResultadoOpcion();
		rseleccionada.setOpcion(seleccionada);		
		rseleccionada.setPregunta(pregunta);
		return rseleccionada;
	}

	public void setExamenId(int examenId) {
		ya_hizo_examen = resultadoService.existeporusuarioyexamen(usuario.getId(), examenId) != null;
		this.examenId = examenId;
		
		if (ya_hizo_examen) { return; }
		
		this.examen = examenService.obtener(examenId);
		this.examen.setPreguntaExamens(preguntaExamenService.detallexexamen(examenId));
		
		resultadoOpciones = new ArrayList<ResultadoOpcion>();
		for (PreguntaExamen prgExa : examen.getPreguntaExamens()) {
			prgExa.getPregunta().setOpcions(opcionService.opcionesxpregunta(prgExa.getPregunta().getId()));
			ResultadoOpcion ro = new ResultadoOpcion();
			ro.setPregunta(prgExa.getPregunta());
			int idTP = prgExa.getPregunta().getTipoPregunta().getId();
			if (idTP == 3) {
				resultadoOpciones.add(ro);
			} else {
				ro.setPseudoOpcion(new Opcion());
				resultadoOpciones.add(ro);
			}
		}		
		
		// set timer por la duracion del examen.
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(this.examen.getDuracion());
		int horaSegundos = calendar.get(Calendar.HOUR) * 60 * 60;
		int minSegundos = calendar.get(Calendar.MINUTE) * 60;
		int segundos = calendar.get(Calendar.SECOND);
		this.duracionExamen = (horaSegundos + minSegundos + segundos);
	}
	
	public Examen getExamen() {
		return examen;
	}

	public void setExamen(Examen examen) {
		this.examen = examen;
	}

	public List<ResultadoOpcion> getResultadoOpciones() {
		return resultadoOpciones;
	}

	public void setResultadoOpciones(List<ResultadoOpcion> resultadoOpciones) {
		this.resultadoOpciones = resultadoOpciones;
	}

	public int getDuracionExamen() {
		return duracionExamen;
	}

	public void setDuracionExamen(int duracionExamen) {
		this.duracionExamen = duracionExamen;
	}

	public int getTiempoUsado() {
		return tiempoUsado;
	}

	public void setTiempoUsado(int tiempoUsado) {
		this.tiempoUsado = tiempoUsado;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public boolean isYa_hizo_examen() {
		return ya_hizo_examen;
	}

	public void setYa_hizo_examen(boolean ya_hizo_examen) {
		this.ya_hizo_examen = ya_hizo_examen;
	}

}
