package com.cibertec.examenlinea.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;

import com.cibertec.examenlinea.bean.Examen;
import com.cibertec.examenlinea.bean.Resultado;
import com.cibertec.examenlinea.bean.Usuario;
import com.cibertec.examenlinea.bean.UsuarioCurso;
import com.cibertec.examenlinea.service.ExamenService;
import com.cibertec.examenlinea.service.ResultadoService;
import com.cibertec.examenlinea.service.UsuarioCursoService;
import com.cibertec.examenlinea.util.Menus;

@SessionScoped
@ManagedBean(name = "menuController")
public class MenuController {

    private MenuModel model;
	private ExamenService examenService = new ExamenService();
	private UsuarioCursoService usuarioCursoService = new UsuarioCursoService();
	private ResultadoService resultadoService = new ResultadoService();
	private List<Examen> examenesHoy;
	private Usuario usuario;
    
    @PostConstruct
    public void init() {
        model = new DefaultMenuModel();
        usuario = (Usuario) FacesContext.getCurrentInstance()
				.getExternalContext().getSessionMap().get("usuario");
        examenesHoy = new ArrayList<Examen>();
        if (usuario.getRol().equals("E")) {
    		List<UsuarioCurso> cursos = usuarioCursoService.listaporusuario(usuario.getId());
    		for (UsuarioCurso usuarioCurso : cursos) {
    			List<Examen> examens = examenService.examenesxcurso(usuarioCurso.getCurso().getId());
    			for (Examen examen : examens) {
    				Resultado exist = resultadoService.existeporusuarioyexamen(usuario.getId(), examen.getId());
    				if(!examen.getFechaInicio().after(usuario.getCurrentDate()) && 
    				   !examen.getFechaFin().before(usuario.getCurrentDate()) && (exist == null)) {
    					examenesHoy.add(examen);
    				}
    			}
    		}   		

        }
        this.establecerPermisos();
    }
    
    public void establecerPermisos() {
		
        DefaultMenuItem home = new DefaultMenuItem();
        home.setUrl("/pages/principal.jsf");
        home.setIcon("ui-icon-home");
        model.addElement(home);
        
        for (String[] menu : Menus.MENUS) {
			if(menu[2].equals(usuario.getRol())) {
				DefaultMenuItem item = new DefaultMenuItem(menu[0]);
                item.setUrl(menu[1]);
                model.addElement(item);
			}
		}
        DefaultSubMenu settings = new DefaultSubMenu();
        settings.setLabel(usuario.getUsuario());
        settings.setStyle("float:right;");
        
        // Cerrar sesion        
		DefaultMenuItem perfil = new DefaultMenuItem("Perfil");
		perfil.setUrl("/pages/perfil.jsf");
        settings.addElement(perfil);
        
		DefaultMenuItem logOut = new DefaultMenuItem("Cerrar Sesión");
		logOut.setCommand("#{authController.logOut}");
		logOut.setIcon("ui-icon-power");
		logOut.setAjax(false);
        settings.addElement(logOut);

        model.addElement(settings);
        
        if (usuario.getRol().equals("E")) {
        	if (examenesHoy.size() > 0) {

                DefaultSubMenu examens = new DefaultSubMenu();
                examens.setLabel("Examenes pendiente");
                examens.setStyle("float:right;");
        		for (Examen examen : examenesHoy) {
        			DefaultMenuItem ex = new DefaultMenuItem(examen.getNombre());
        			ex.setUrl("/pages/estudiante/curso/show.jsf?cursoId=" + examen.getCurso().getId() );
        			examens.addElement(ex);
        		}                
                model.addElement(examens);
        	}
        }
        
        
    }

	public MenuModel getModel() {
		return model;
	}

	public void setModel(MenuModel model) {
		this.model = model;
	}

	public List<Examen> getExamenesHoy() {
		return examenesHoy;
	}

	public void setExamenesHoy(List<Examen> examenesHoy) {
		this.examenesHoy = examenesHoy;
	}

}
