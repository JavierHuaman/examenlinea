package com.cibertec.examenlinea.controller;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;

import com.cibertec.examenlinea.bean.Usuario;
import com.cibertec.examenlinea.service.UsuarioService;

@ViewScoped
@ManagedBean(name = "usuarioController")
public class UsuarioController {
	
	private UsuarioService usuarioService = new UsuarioService();
	private Usuario usuario;
	private List<Usuario> usuarios;
	
	private int usuarioId;
	
	@PostConstruct
	public void init() {
		usuario = new Usuario();
		usuarios = usuarioService.listar();
	}
	
	public String registrar() {
		String redirect = null;
		try {
			usuario = usuarioService.registrar(usuario);
			FacesContext context = FacesContext.getCurrentInstance();
			context.getExternalContext().getFlash().setKeepMessages(true);
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Aviso", 
							"Se registro satisfactioramente."));
			redirect = "/pages/admin/usuario/editar?faces-redirect=true&usuarioId=" + usuario.getId();	
		} catch (Exception e) {;
			FacesContext.getCurrentInstance()
				.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Aviso", "Error al registrar."));
		}
		return redirect;
	}
	
	public String actualizar() {
		String redirect = null;
		try {
			usuario = usuarioService.actualizar(usuario);
			FacesContext context = FacesContext.getCurrentInstance();
			context.getExternalContext().getFlash().setKeepMessages(true);
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Aviso", 
							"Se registro satisfactioramente."));
			redirect = "/pages/admin/usuario/editar?faces-redirect=true&usuarioId=" + usuario.getId();	
		} catch (Exception e) {;
			FacesContext.getCurrentInstance()
				.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Aviso", "Error al registrar."));
		}
		return redirect;
	}
	
	public String registrarEstudiante() {
		String redirect = null;
		try {
			usuario.setRol("E");
			usuario.setEstado("1");
			usuario = usuarioService.registrar(usuario);
			FacesContext.getCurrentInstance()
				.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Aviso", 
							"Se registro satisfactioramente."));
			FacesContext.getCurrentInstance()
				.getExternalContext()
					.getSessionMap().put("usuario", usuario);
			redirect = "/pages/principal?faces-redirect=true";	
		} catch (Exception e) {;
			FacesContext.getCurrentInstance()
				.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Aviso", "Error al registrar."));
		}
		
		return redirect;
	}
	
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<Usuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	public int getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(int usuarioId) {
		usuario = usuarioService.obtener(usuarioId);
		this.usuarioId = usuarioId;
	}
	
}
