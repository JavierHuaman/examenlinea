package com.cibertec.examenlinea.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;

import org.primefaces.component.api.UIData;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.primefaces.context.RequestContext;

import com.cibertec.examenlinea.bean.Curso;
import com.cibertec.examenlinea.bean.Examen;
import com.cibertec.examenlinea.bean.Opcion;
import com.cibertec.examenlinea.bean.Pregunta;
import com.cibertec.examenlinea.bean.PreguntaExamen;
import com.cibertec.examenlinea.bean.TipoPregunta;
import com.cibertec.examenlinea.service.CursoService;
import com.cibertec.examenlinea.service.ExamenService;
import com.cibertec.examenlinea.service.OpcionService;
import com.cibertec.examenlinea.service.PreguntaExamenService;
import com.cibertec.examenlinea.service.PreguntaService;
import com.cibertec.examenlinea.service.TipoPreguntaService;
import com.cibertec.examenlinea.util.DateConverter;

@SessionScoped
@ManagedBean(name = "examenController")
public class ExamenController {
	
	private ExamenService examenService = new ExamenService();
	private CursoService cursoService = new CursoService();
	private PreguntaService preguntaService = new PreguntaService();
	private TipoPreguntaService tipoPreguntaService = new TipoPreguntaService();
	private PreguntaExamenService preguntaExamenService = new PreguntaExamenService();
	private OpcionService opcionService = new OpcionService();
	
	private int cursoId;
	private int examenId;
	private String accionPreg; // si se va a crear uno nuevo o modificar.
	private int currentIndex;
	private String txtBancoPregunta;
	private List<Pregunta> bancoPreguntas;
	private int txtPuntos;
	
	private Examen examen;
	private UIData dttExamenes;
	private Curso curso;
	private List<PreguntaExamen> preguntas;
	private List<PreguntaExamen> preguntaEliminadas;
	private List<TipoPregunta> tipoPreguntas;
	private PreguntaExamen preguntaEx;
	
	// Agregar pregunta.
	private int idTP;
	
	// respuestas
	private String verdaderoFalso; // si el tipo de pregunta es verdadero falso.
	private String rptCompleta; // si el tipo de pregunta es texto. (o como alternativa)
	private List<String> alternativas; // si el tipo de pregunta es seleccion unica o multiple.
	private String alternativa;
	private List<String> alternativasSeleccionadas;
	
	// Para Fechas
	private String today;
	private String fechaInicio;
	
	@PostConstruct
	public void init() {
		examen = new Examen();
		curso = new Curso();
		preguntaEx = new PreguntaExamen();
		preguntas = new ArrayList<PreguntaExamen>();
		tipoPreguntas = tipoPreguntaService.listar();
		alternativas = new ArrayList<String>();
		alternativasSeleccionadas = new ArrayList<String>();
		preguntaEliminadas = new ArrayList<PreguntaExamen>();
		
		today = DateConverter.toString(new Date());
	}
	
	public String registrar() {
		String redirect = null;
		try {
			if (preguntas.isEmpty()) {
				FacesContext.getCurrentInstance()
					.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Aviso", "Agregue preguntas al examen."));
				return redirect;
			}
			examen.setCurso(curso);
			examen = examenService.registrar(examen);
			for (PreguntaExamen prgEx : preguntas) {
				Pregunta prg = prgEx.getPregunta();
				if (prg.getId() == 0) {
					List<Opcion> opciones = prg.getOpcions();
					prg.setOpcions(null);
					prg = preguntaService.registrar(prg);
					for (Opcion opt : opciones) {
						opt.setPregunta(prg);
						opcionService.registrar(opt);
					}					
				}
				prgEx.setPregunta(prg);
				prgEx.setExamen(examen);
				preguntaExamenService.registrar(prgEx);
			}
			FacesContext context = FacesContext.getCurrentInstance();
			context.getExternalContext().getFlash().setKeepMessages(true);
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Aviso", 
							"Se registro satisfactioramente."));
			redirect = "/pages/admin/curso/examen/editar?faces-redirect=true&examenId=" + examen.getId();
		} catch (Exception e) {
			e.printStackTrace();
			FacesContext.getCurrentInstance()
				.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Aviso", "Error al registrar."));
		}
		return redirect;
	}
	
	public String actualizar() {
		String redirect = null;
		try {
			for (PreguntaExamen pEx : preguntaEliminadas) {
				preguntaExamenService.eliminar(pEx.getId());
			}			
			examen = examenService.actualizar(examen);
			for (PreguntaExamen prgEx : preguntas) {
				if (prgEx.getId() == 0) {
					Pregunta prg = prgEx.getPregunta();
					if (prg.getId() == 0) {
						List<Opcion> opciones = prg.getOpcions();
						prg.setOpcions(null);
						prg = preguntaService.registrar(prg);
						for (Opcion opt : opciones) {
							opt.setPregunta(prg);
							opcionService.registrar(opt);
						}					
					}
					prgEx.setPregunta(prg);
					prgEx.setExamen(examen);
					preguntaExamenService.registrar(prgEx);					
				}
			}
			FacesContext context = FacesContext.getCurrentInstance();
			context.getExternalContext().getFlash().setKeepMessages(true);
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Aviso", 
							"Se registro satisfactioramente."));
			redirect = "/pages/admin/curso/examen/editar?faces-redirect=true&examenId=" + examen.getId();
		} catch (Exception e) {
			e.printStackTrace();
			FacesContext.getCurrentInstance()
				.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Aviso", "Error al actualizar."));
		}
		return redirect;
	}
	
	public String eliminar() {
		String redirect = null;
		try {
			int cursoId = curso.getId();			
			examenService.eliminar(examen.getId());	
			FacesContext.getCurrentInstance()
				.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Aviso", "Eliminado correctamente."));
			redirect = "/pages/admin/curso/editar?faces-redirect=true&cursoId=" + cursoId;
		} catch (Exception e) {
			FacesContext.getCurrentInstance()
				.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Aviso", "Error al eliminar."));
		}
		return redirect;
	}
	
	public void nuevaPregunta() {
		this.setAccionPreg("R");
		this.preguntaEx = new PreguntaExamen();
		this.idTP = 0;
		this.verdaderoFalso = null;
		this.rptCompleta = null;
		this.alternativas = new ArrayList<String>();
		this.alternativasSeleccionadas = new ArrayList<String>();
		
		RequestContext req = RequestContext.getCurrentInstance();
		req.execute("PF('wdglPregunta').show();");
	}
	
	public void verPregunta(PreguntaExamen preguntaEx, int currentIndex) {
		this.currentIndex = currentIndex;
		this.preguntaEx = preguntaEx;
		
		RequestContext req = RequestContext.getCurrentInstance();
		req.execute("PF('wdglEliminaPregunta').show();");
	}
	
	public void agregarPregunta() {
		Pregunta preg = preguntaEx.getPregunta();
		preg.setCurso(curso);
		preg.setTipoPregunta(tipoPreguntaService.obtener(preg.getTipoPregunta().getId()));
		
		switch (idTP) {
			case 1:
				String[] opciones = {"Verdadero", "Falso"};
				for (String op : opciones) {
					Opcion opcion = new Opcion();
					opcion.setDescripcion(op);
					opcion.setEsCorrecto(verdaderoFalso.equals(op.toLowerCase()));
					preg.addOpcion(opcion);			
				}			
				break;
			case 2:
				for (String alt : alternativas) {
					Opcion opcion = new Opcion();
					opcion.setDescripcion(alt);
					opcion.setEsCorrecto(alternativa.equals(alt));
					preg.addOpcion(opcion);	
				}
				break;
			case 3:
				for (String alt : alternativas) {
					Opcion opcion = new Opcion();
					opcion.setDescripcion(alt);
					opcion.setEsCorrecto(alternativasSeleccionadas.indexOf(alt) != -1);
					preg.addOpcion(opcion);	
				}				
				break;
			case 4:
				Opcion opcion = new Opcion();
				opcion.setDescripcion(rptCompleta);
				opcion.setEsCorrecto(true);
				preg.addOpcion(opcion);	
			default:
				break;
		}
		
		preguntaEx.setPregunta(preg);
		preguntas.add(preguntaEx);

		RequestContext req = RequestContext.getCurrentInstance();
		req.execute("PF('wdglPregunta').hide();");
	}

	public void eliminarPregunta() {
		if (preguntaEx.getId() != 0) {
			preguntaEliminadas.add(preguntaEx);
		}
		preguntas.remove(currentIndex);

		RequestContext req = RequestContext.getCurrentInstance();
		req.execute("PF('wdglEliminaPregunta').hide();");
	}
	
	public void abrirBancoPregunta() {
		bancoPreguntas = preguntaService.detallexcurso(curso.getId());
		txtBancoPregunta = null ;
		txtPuntos = 0;
		
		RequestContext req = RequestContext.getCurrentInstance();
		req.execute("PF('wdglBancoPregunta').show();");
	}
	
	public void obtenerBancoPregunta(int idPregunta) {
		Pregunta prg = preguntaService.obtener(idPregunta);
		PreguntaExamen prgEx = new PreguntaExamen();
		prgEx.setPregunta(prg);
		prgEx.setPuntos(txtPuntos);
		preguntas.add(prgEx);
		
		RequestContext req = RequestContext.getCurrentInstance();
		req.execute("PF('wdglBancoPregunta').hide();");
	}
	
	
	// Evento del select tipo de pregunta
	public void selectionChanged(final AjaxBehaviorEvent event) {
		SelectOneMenu oneMenu = (SelectOneMenu) event.getSource();
		this.idTP = Integer.parseInt(oneMenu.getValue().toString());
		this.verdaderoFalso = null;
		this.rptCompleta = null;
		this.alternativas = new ArrayList<String>();
		this.alternativasSeleccionadas = new ArrayList<String>();
	}
	
	public void bancoPreguntaKeyUp() {
		bancoPreguntas = preguntaService.detallexdescripcion(txtBancoPregunta);
	}
		
	public void agregarAlternativa() {
		alternativas.add(rptCompleta);
		this.rptCompleta = null;
	}

	public void eliminarAlternativa(int index) {
		alternativas.remove(index);
	}
	
	public void onDateSelect() {
		fechaInicio = DateConverter.toString(examen.getFechaInicio());
	}
	
	
	//---------------------------------//
	// Getters y setters
	//---------------------------------//
	public Examen getExamen() {
		return examen;
	}

	public void setExamen(Examen examen) {
		this.examen = examen;
	}
	
	public UIData getDttExamenes() {
		return dttExamenes;
	}

	public void setDttExamenes(UIData dttExamenes) {
		this.dttExamenes = dttExamenes;
	}

	public int getCursoId() {
		return cursoId;
	}
	
	// Recepcionar parametro
	public void setCursoId(int cursoId) {
		this.curso = cursoService.obtener(cursoId);
		this.examen = new Examen();
		this.preguntas = new ArrayList<PreguntaExamen>();
		this.cursoId = cursoId;
		this.preguntaEliminadas = new ArrayList<PreguntaExamen>();
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	public List<PreguntaExamen> getPreguntas() {
		return preguntas;
	}

	public void setPreguntas(List<PreguntaExamen> preguntas) {
		this.preguntas = preguntas;
	}

	public int getExamenId() {
		return examenId;
	}
	
	// Setter examen por el id
	public void setExamenId(int examenId) {
		this.examen = examenService.obtener(examenId);
		this.curso = examen.getCurso();
		this.preguntas = preguntaExamenService.detallexexamen(examen.getId());
		this.preguntaEliminadas = new ArrayList<PreguntaExamen>();
		
		for (PreguntaExamen prg : preguntas) {
			prg.getPregunta().setOpcions(opcionService.opcionesxpregunta(prg.getPregunta().getId()));
		}
		
		this.examenId = examenId;
	}

	public String getAccionPreg() {
		return accionPreg;
	}

	public void setAccionPreg(String accionPreg) {
		this.accionPreg = accionPreg;
	}

	public PreguntaExamen getPreguntaEx() {
		return preguntaEx;
	}

	public void setPreguntaEx(PreguntaExamen preguntaEx) {
		this.preguntaEx = preguntaEx;
	}

	public List<TipoPregunta> getTipoPreguntas() {
		return tipoPreguntas;
	}

	public void setTipoPreguntas(List<TipoPregunta> tipoPreguntas) {
		this.tipoPreguntas = tipoPreguntas;
	}

	public int getIdTP() {
		return idTP;
	}

	public void setIdTP(int idTP) {
		this.idTP = idTP;
	}

	public String getVerdaderoFalso() {
		return verdaderoFalso;
	}

	public void setVerdaderoFalso(String verdaderoFalso) {
		this.verdaderoFalso = verdaderoFalso;
	}

	public String getRptCompleta() {
		return rptCompleta;
	}

	public void setRptCompleta(String rptCompleta) {
		this.rptCompleta = rptCompleta;
	}

	public int getCurrentIndex() {
		return currentIndex;
	}

	public void setCurrentIndex(int currentIndex) {
		this.currentIndex = currentIndex;
	}

	public List<String> getAlternativas() {
		return alternativas;
	}

	public void setAlternativas(List<String> alternativas) {
		this.alternativas = alternativas;
	}

	public String getAlternativa() {
		return alternativa;
	}

	public void setAlternativa(String alternativa) {
		this.alternativa = alternativa;
	}

	public List<String> getAlternativasSeleccionadas() {
		return alternativasSeleccionadas;
	}

	public void setAlternativasSeleccionadas(List<String> alternativasSeleccionadas) {
		this.alternativasSeleccionadas = alternativasSeleccionadas;
	}

	public List<PreguntaExamen> getPreguntaEliminadas() {
		return preguntaEliminadas;
	}

	public void setPreguntaEliminadas(List<PreguntaExamen> preguntaEliminadas) {
		this.preguntaEliminadas = preguntaEliminadas;
	}

	public String getTxtBancoPregunta() {
		return txtBancoPregunta;
	}

	public void setTxtBancoPregunta(String txtBancoPregunta) {
		this.txtBancoPregunta = txtBancoPregunta;
	}

	public List<Pregunta> getBancoPreguntas() {
		return bancoPreguntas;
	}

	public void setBancoPreguntas(List<Pregunta> bancoPreguntas) {
		this.bancoPreguntas = bancoPreguntas;
	}

	public int getTxtPuntos() {
		return txtPuntos;
	}

	public void setTxtPuntos(int txtPuntos) {
		this.txtPuntos = txtPuntos;
	}

	public String getToday() {
		return today;
	}

	public void setToday(String today) {
		this.today = today;
	}

	public String getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	
}
