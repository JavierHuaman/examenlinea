package com.cibertec.examenlinea.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.ScheduleModel;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;

import com.cibertec.examenlinea.bean.Curso;
import com.cibertec.examenlinea.bean.Examen;
import com.cibertec.examenlinea.bean.Resultado;
import com.cibertec.examenlinea.bean.Usuario;
import com.cibertec.examenlinea.bean.UsuarioCurso;
import com.cibertec.examenlinea.service.CursoService;
import com.cibertec.examenlinea.service.ExamenService;
import com.cibertec.examenlinea.service.ResultadoService;
import com.cibertec.examenlinea.service.UsuarioCursoService;

@ViewScoped
@ManagedBean(name = "principalController")
public class PrincipalController {

	private UsuarioCursoService usuarioCursoService = new UsuarioCursoService();
	private CursoService cursoService = new CursoService();
	private ExamenService examenService = new ExamenService();
	private ResultadoService resultadoService = new ResultadoService();
	private ScheduleModel eventModel;
	
	private Usuario usuario;
	private List<Curso> cursos;
	
	private BarChartModel barModel;
	
	@PostConstruct
	public void init() {
		usuario = (Usuario) FacesContext.getCurrentInstance()
								.getExternalContext()
								.getSessionMap().get("usuario");
		if (usuario.getRol().equals("A")) {
			cursos = cursoService.listarActivos();
			barModel = new BarChartModel();
			ChartSeries aprobados = new ChartSeries();
			aprobados.setLabel("Aprobados");
			
			ChartSeries desaprobados = new ChartSeries();
			desaprobados.setLabel("Desaprobados");

			int mayor = 0;
			for (Curso curso : cursos) {
				List<Resultado> resultados = resultadoService.existeporcursooexamen(curso.getId(), 0);
				
				int totalAprobados = 0;
				int totalDesaprobados = 0;
				for (Resultado resultado : resultados) {
					if (resultado.getEstado().equals("A")) {
						totalAprobados ++;
					} else {
						totalDesaprobados ++;
					}
				}
				aprobados.set(curso.getNombre(), totalAprobados);
				desaprobados.set(curso.getNombre(), totalDesaprobados);
				if (totalAprobados > mayor) { 
					mayor = totalAprobados;
				} else if(totalDesaprobados > mayor) {
					mayor = totalDesaprobados;
				}
				
			}

			barModel.addSeries(aprobados);
			barModel.addSeries(desaprobados);
			barModel.setTitle("Aprob./Desprob. por Curso");

	        barModel.setLegendPosition("ne");
	         
	        Axis xAxis = barModel.getAxis(AxisType.X);
	        xAxis.setLabel("Curso");
	         
	        Axis yAxis = barModel.getAxis(AxisType.Y);
	        yAxis.setLabel("Total");
	        yAxis.setMin(0);
	        yAxis.setMax((mayor + 1));
			
		} else {
			List<UsuarioCurso> usuarioCursos = usuarioCursoService.listaporusuario(usuario.getId());
			cursos = new ArrayList<Curso>();
			for (UsuarioCurso usuarioCurso : usuarioCursos) {
				cursos.add(usuarioCurso.getCurso());
			}
			
	        eventModel = new DefaultScheduleModel();
	        
	        for (Curso curso : cursos) {
				List<Examen> examenes = examenService.examenesxcurso(curso.getId());
				for (Examen examen : examenes) {
					eventModel.addEvent(new DefaultScheduleEvent(curso.getNombre() +  " - " + examen.getNombre(), 
																 examen.getFechaInicio(), 
																 examen.getFechaFin()));				
				}
		         
			}			
		}
        
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<Curso> getCursos() {
		return cursos;
	}

	public void setCursos(List<Curso> cursos) {
		this.cursos = cursos;
	}

	public ScheduleModel getEventModel() {
		return eventModel;
	}

	public void setEventModel(ScheduleModel eventModel) {
		this.eventModel = eventModel;
	}

	public BarChartModel getBarModel() {
		return barModel;
	}

	public void setBarModel(BarChartModel barModel) {
		this.barModel = barModel;
	}
	
}
