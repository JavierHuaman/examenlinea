package com.cibertec.examenlinea.controller;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import org.primefaces.component.api.UIData;
import org.primefaces.context.RequestContext;

import com.cibertec.examenlinea.bean.Curso;
import com.cibertec.examenlinea.bean.Examen;
import com.cibertec.examenlinea.service.CursoService;
import com.cibertec.examenlinea.service.ExamenService;

@RequestScoped
@ManagedBean(name = "cursoController")
public class CursoController {
	
	private CursoService cursoService = new CursoService();
	private ExamenService examenService = new ExamenService();
	private Curso curso;
	private List<Curso> cursos;
	// Permite obtener el elemento selecionado del datatable del mantenimiento
	
	private UIData dttCursos;
	private int cursoId;
	private Examen examen;
	
	@PostConstruct
	public void init() {
		curso = new Curso();
		cursos = cursoService.listar();
	}

	public String registrar() {
		String redirect = null;
		try {
			curso.setEstado("0");
			curso = cursoService.registrar(curso);
			FacesContext context = FacesContext.getCurrentInstance();
			context.getExternalContext().getFlash().setKeepMessages(true);
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Aviso", 
							"Se registro satisfactioramente."));
			redirect = "/pages/admin/curso/editar?faces-redirect=true&cursoId=" + curso.getId();
		} catch (Exception e) {
			FacesContext.getCurrentInstance()
			.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Aviso", "Error al grabar los datos"));
		}
		return redirect;
	}
	
	public String editar() {
		// getRowData(); --> Obtiene el objeto seleccionado
		curso = (Curso) dttCursos.getRowData();
		return "/pages/admin/curso/editar"; 
	}
	
	public String actualizar() {
		String redirect = null;
		try {
			curso = cursoService.actualizar(curso);

			FacesContext context = FacesContext.getCurrentInstance();
			context.getExternalContext().getFlash().setKeepMessages(true);
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Aviso", 
							"Se registro satisfactioramente."));
			redirect = "/pages/admin/curso/editar?faces-redirect=true&cursoId=" + curso.getId();
		} catch (Exception e) {
			FacesContext.getCurrentInstance()
			.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Aviso", "Error al grabar los datos"));
		}
		return redirect;
	}
	
	public void dialogCurso(int idCurso, String estado) {
		this.curso = cursoService.obtener(idCurso);
		RequestContext req = RequestContext.getCurrentInstance();
		req.execute("PF('" + (estado.equals("0") ? "wdlgActivarCurso" : "wdlgDesactivarCurso") + "').show();");
	}

	public void activarCurso() {
		try {
			curso = cursoService.obtener(curso.getId());
			curso.setEstado("1");
			cursoService.actualizar(curso);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		RequestContext req = RequestContext.getCurrentInstance();
		req.execute("PF('wdlgActivarCurso').hide();");
	}

	public void desactivarCurso() {
		try {
			curso = cursoService.obtener(curso.getId());
			curso.setEstado("0");
			cursoService.actualizar(curso);
		} catch (Exception e) {
			e.printStackTrace();
		}
		RequestContext req = RequestContext.getCurrentInstance();
		req.execute("PF('wdlgDesactivarCurso').hide();");
	}
	
	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	public List<Curso> getCursos() {
		return cursos;
	}

	public void setCursos(List<Curso> cursos) {
		this.cursos = cursos;
	}	

	public UIData getDttCursos() {
		return dttCursos;
	}

	public void setDttCursos(UIData dttCursos) {
		this.dttCursos = dttCursos;
	}

	public int getCursoId() {
		return cursoId;
	}

	public void setCursoId(int cursoId) {
		this.curso = cursoService.obtener(cursoId);
		this.curso.setExamens(examenService.examenesxcurso(curso.getId()));
		this.cursoId = cursoId;
	}

	public Examen getExamen() {
		return examen;
	}

	public void setExamen(Examen examen) {
		this.examen = examen;
	}

}
