package com.cibertec.examenlinea.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.cibertec.examenlinea.bean.Opcion;
import com.cibertec.examenlinea.bean.Pregunta;
import com.cibertec.examenlinea.bean.PreguntaExamen;
import com.cibertec.examenlinea.bean.Resultado;
import com.cibertec.examenlinea.bean.ResultadoOpcion;
import com.cibertec.examenlinea.service.PreguntaExamenService;
import com.cibertec.examenlinea.service.ResultadoOpcionService;
import com.cibertec.examenlinea.service.ResultadoService;

@RequestScoped
@ManagedBean(name = "usuarioresultadoController")
public class UsuarioResultadoController {
	
	private int resultadoId;
	private Resultado resultado;
	private ResultadoService resultadoService = new ResultadoService();
	private ResultadoOpcionService resultadoOpcionService = new ResultadoOpcionService();
	private PreguntaExamenService preguntaExamenService = new PreguntaExamenService();

	private List<ResultadoOpcion> resultadoOpciones;
	private int acertado;
	private int respuestaCorrecta;
	
	@PostConstruct
	public void init() {
		resultadoOpciones = new ArrayList<ResultadoOpcion>();
	}
	
	public int getResultadoId() {
		return resultadoId;
	}

	public void setResultadoId(int resultadoId) {
		this.resultado = resultadoService.obtener(resultadoId);
		this.resultado.getExamen().setPreguntaExamens(preguntaExamenService.detallexexamen(resultado.getExamen().getId()));
		resultado.setResultadoOpcions(resultadoOpcionService.detallexresultado(resultadoId));
		Iterator<ResultadoOpcion> rOs = resultado.getResultadoOpcions().iterator();
		List<String> idPreguntas = new ArrayList<String>();
		while(rOs.hasNext()) {
			ResultadoOpcion resultadoOpcion = rOs.next();
			List<String> multiOpcion = new ArrayList<String>();
			Pregunta pregunta = resultadoOpcion.getPregunta();
			int idTipoPregunta = pregunta.getTipoPregunta().getId();
			if (idTipoPregunta == 3) {
				for (ResultadoOpcion rO2 : resultado.getResultadoOpcions()) {
					if (rO2.getPregunta().getId() == pregunta.getId()) {
						multiOpcion.add(String.valueOf(rO2.getOpcion().getId()));						
					}
				}
				resultadoOpcion.setMultiOpcion(multiOpcion);
			} else if (idTipoPregunta == 4) {
				Opcion opcion = new Opcion();
				opcion.setDescripcion(resultadoOpcion.getOpcionTexto());
				resultadoOpcion.setPseudoOpcion(opcion);			
			}  
			else {
				resultadoOpcion.setPseudoOpcion(resultadoOpcion.getOpcion());
			}
			if (!idPreguntas.contains(String.valueOf(pregunta.getId()))) {
				resultadoOpciones.add(resultadoOpcion);
				idPreguntas.add(String.valueOf(pregunta.getId()));
			}
		}
		
		this.acertado = 0;
		this.respuestaCorrecta = 0;
		
		// TODO
		for (ResultadoOpcion resultadoOpcion: resultadoOpciones) {
			Pregunta pregunta = resultadoOpcion.getPregunta();
			int idTipoPregunta = pregunta.getTipoPregunta().getId();
			List<Opcion> opcionesCorrectas = pregunta.opcionesCorrectas();
			Opcion correcto = null;
			switch (idTipoPregunta) {
				case 1: // verdadero o falso
					correcto = opcionesCorrectas.get(0);
					if(correcto.getId() == resultadoOpcion.getPseudoOpcion().getId()) {
						PreguntaExamen pEx = preguntaExamenService.obtenerporexamenypregunta(resultado.getExamen().getId(), pregunta.getId());
						acertado += pEx.getPuntos();
						resultadoOpcion.setCorrecto(true);
						respuestaCorrecta +=1;
					}
					break;
				case 2: // seleccion unica
					correcto = opcionesCorrectas.get(0);
					if(correcto.getId() == resultadoOpcion.getPseudoOpcion().getId()) {
						PreguntaExamen pEx = preguntaExamenService.obtenerporexamenypregunta(resultado.getExamen().getId(), pregunta.getId());
						acertado += pEx.getPuntos();
						resultadoOpcion.setCorrecto(true);
						respuestaCorrecta +=1;
					}		
					break;
				case 3: // seleccion multiple
					boolean es_correcto = false;
					for (String sidOpc : resultadoOpcion.getMultiOpcion()) {
						int idOpc = Integer.parseInt(sidOpc);
						for (Opcion opcionCorrecta : opcionesCorrectas) {
							if (idOpc == opcionCorrecta.getId()) {
								es_correcto = true;
								break;
							}
						}
						Opcion opt = new Opcion();
						opt.setId(idOpc);
					}
					if (es_correcto) {
						PreguntaExamen pEx = preguntaExamenService.obtenerporexamenypregunta(resultado.getExamen().getId(), pregunta.getId());
						acertado += pEx.getPuntos();	
						resultadoOpcion.setCorrecto(true);
						respuestaCorrecta +=1;					
					}
					break;
				case 4: // texto
					correcto = opcionesCorrectas.get(0);
					if(correcto.getDescripcion().equalsIgnoreCase(resultadoOpcion.getPseudoOpcion().getDescripcion())) {
						PreguntaExamen pEx = preguntaExamenService.obtenerporexamenypregunta(resultado.getExamen().getId(), pregunta.getId());
						acertado += pEx.getPuntos();
						resultadoOpcion.setCorrecto(true);
						respuestaCorrecta +=1;
					}
					break;	
				default:
					break;
			}
		}
		
		
		this.resultadoId = resultadoId;
	}

	public Resultado getResultado() {
		return resultado;
	}

	public void setResultado(Resultado resultado) {
		this.resultado = resultado;
	}

	public List<ResultadoOpcion> getResultadoOpciones() {
		return resultadoOpciones;
	}

	public void setResultadoOpciones(List<ResultadoOpcion> resultadoOpciones) {
		this.resultadoOpciones = resultadoOpciones;
	}

	public int getAcertado() {
		return acertado;
	}

	public void setAcertado(int acertado) {
		this.acertado = acertado;
	}

	public int getRespuestaCorrecta() {
		return respuestaCorrecta;
	}

	public void setRespuestaCorrecta(int respuestaCorrecta) {
		this.respuestaCorrecta = respuestaCorrecta;
	}

}
