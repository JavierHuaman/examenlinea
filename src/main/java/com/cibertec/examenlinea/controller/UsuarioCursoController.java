package com.cibertec.examenlinea.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import com.cibertec.examenlinea.bean.Curso;
import com.cibertec.examenlinea.bean.Examen;
import com.cibertec.examenlinea.bean.Resultado;
import com.cibertec.examenlinea.bean.Usuario;
import com.cibertec.examenlinea.bean.UsuarioCurso;
import com.cibertec.examenlinea.service.CursoService;
import com.cibertec.examenlinea.service.ExamenService;
import com.cibertec.examenlinea.service.ResultadoService;
import com.cibertec.examenlinea.service.UsuarioCursoService;

@RequestScoped
@ManagedBean(name = "usuariocursoController")
public class UsuarioCursoController {

	private CursoService cursoService = new CursoService();
	private ExamenService examenService = new ExamenService();
	private UsuarioCursoService usuarioCursoService = new UsuarioCursoService();
	private ResultadoService resultadoService = new ResultadoService();
	
	private int cursoId;
	
	private Curso curso;
	private Usuario usuario;
	private boolean usuario_yatiene_curso;
	private List<Curso> cursos;
	private List<Resultado> resultados;
	
	@PostConstruct
	public void init() {
		curso = new Curso();
		cursos = cursoService.listarActivos();
		usuario = (Usuario) FacesContext.getCurrentInstance()
				.getExternalContext()
				.getSessionMap().get("usuario");
		resultados = new ArrayList<Resultado>();
	}
	
	public String agregarMisCursos() {
		String redirect = null;
		try {
			UsuarioCurso usuarioCurso = new UsuarioCurso();
			usuarioCurso.setCurso(curso);
			usuarioCurso.setUsuario(usuario);
			usuarioCursoService.registrar(usuarioCurso);
			FacesContext context = FacesContext.getCurrentInstance();
			context.getExternalContext().getFlash().setKeepMessages(true);
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Aviso", 
							"Se agrego satisfactoriamente."));
			redirect = "/pages/estudiante/curso/show.jsf?faces-redirect=true&cursoId=" + curso.getId();
		} catch(Exception e) {
			e.printStackTrace();
		}
		return redirect;
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	public List<Curso> getCursos() {
		return cursos;
	}

	public void setCursos(List<Curso> cursos) {
		this.cursos = cursos;
	}

	public int getCursoId() {
		return cursoId;
	}

	public void setCursoId(int cursoId) {
		this.curso = cursoService.obtener(cursoId);
		this.curso.setExamens(examenService.examenesxcurso(curso.getId()));
		this.usuario_yatiene_curso = usuarioCursoService.existisusuarioycurso(usuario.getId(), curso.getId());
		
		for (Examen examen : curso.getExamens()) {
			this.resultados.add(resultadoService.existeporusuarioyexamen(usuario.getId(), 
																	examen.getId()));
		}
		
		this.cursoId = cursoId;
	}

	public boolean getUsuario_yatiene_curso() {
		return usuario_yatiene_curso;
	}

	public void setUsuario_yatiene_curso(boolean usuario_yatiene_curso) {
		this.usuario_yatiene_curso = usuario_yatiene_curso;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<Resultado> getResultados() {
		return resultados;
	}

	public void setResultados(List<Resultado> resultados) {
		this.resultados = resultados;
	}

}
