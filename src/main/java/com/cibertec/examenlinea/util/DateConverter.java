package com.cibertec.examenlinea.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateConverter {

	public static String toString(Date date) {
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		return format.format(date);
	}
}
