package com.cibertec.examenlinea.util;

public class Menus {

	public static String[][] MENUS = {
		{"Curso", "/pages/admin/curso/index.jsf", "A"},
		{"Pregunta", "/pages/admin/pregunta/index.jsf", "A"},
		{"Usuario", "/pages/admin/usuario/index.jsf", "A"},
		{"Resultado", "/pages/admin/resultado/index.jsf", "A"},
		{"Curso", "/pages/estudiante/curso/index.jsf", "E"}
	};
}
